calibre-beta-for-oasis


Use the following command to download calibre and install it:

`sudo -v && wget -nv -O- https://bitbucket.org/Taco_Rocket/calibre-beta-for-kindle-oasis/raw/9b76fb50063ef7d4f2fa67b1128d7f3a427421e1/linux-installer.py | sudo python -c "import sys; main=lambda:sys.stderr.write('Download failed\n'); exec(sys.stdin.read()); main()"`


This will install the beta for both 32bit and 64bit Linux users. 

I do not host the calibre download. This is fetched from their site. The only difference from the main script located at their website here: https://calibre-ebook.com/download_linux

Is that I calculated the signature for the files and hard coded them into the script. While not ideal this will allow the download to install. 


Enjoy your new Oasis!